#
# Cookbook:: bitbucket
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.



file 'bitbucket.properties' do
  content "setup.displayName=displayName
  setup.baseUrl= https://bitbucket.yourcompany.com
  setup.license=AAAB...
  setup.sysadmin.username=username
  setup.sysadmin.password=password
  setup.sysadmin.displayName=John Doe
  setup.sysadmin.emailAddress=sysadmin@yourcompany.com
  jdbc.driver=org.postgresql.Driver
  jdbc.url=jdbc:postgresql://localhost:5432/bitbucket
  jdbc.user=bitbucket 
  jdbc.password=bitbucket 
  plugin.mirroring.upstream.url=https://bitbucket.company.com'
"
  action :create
end


execute "bitbucket install" do
  command "sh /home/vagrant/bitbucket/test.sh"
end
