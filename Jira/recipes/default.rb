file "response.varfile" do
  content "rmiPort$Long=8005
  app.jiraHome=/opt/atlassian/jira-home
  app.install.service$Boolean=true
  existingInstallationDir=/opt/JIRA
  sys.confirmedUpdateInstallationString=false
  sys.languageId=en
  sys.installationDir=/opt/atlassian/jira
  executeLauncherAction$Boolean=true
  httpPort$Long=8080
  portChoice=default
"
  action :create
end

#execute "Install Jira Core" do
 # command "sh istalljira.sh"
#end

script "install_something" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
  "wget https://product-downloads.atlassian.com/software/jira/downloads/atlassian-jira-software-7.3.9-x64.bin
  chmod 777 atlassian-jira-software-7.3.9-x64.bin
  ./atlassian-jira-software-7.3.9-x64.bin -q -varfile response.varfile"
  EOH
end


